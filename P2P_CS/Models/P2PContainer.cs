﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2P_CS.Models
{
    class P2PContainer
    {
        public int ContainerID { get; set; }
        public int EventID { get; set; }
        public string P2PPageTypeID { get; set; }
        public string Properties { get; set; }
        public bool IsHero { get; set; }
        public bool IsHeroLocked { get; set; }
        public int SortOrder { get; set; }
        public int ColumnOrder { get; set; }
        public P2PContainerContent Content { get; set; }
    }
}
