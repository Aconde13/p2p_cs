﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2P_CS.Models
{
    class P2PWidgetType
    {
        public string P2PWidgetTypeID { get; set; }
        public string Documentation { get; set; }
    }
}
