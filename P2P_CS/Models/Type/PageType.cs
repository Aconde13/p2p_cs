﻿namespace P2P_CS.Models.Type
{
    public enum PageType
    {
        Event,
        Donation,
        DonationThankYou

    }
}