﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2P_CS.Models
{
    class P2PWidgetContent
    {
        public int WidgetID { get; set; }
        public string LanguageCode { get; set; }
        public string Content { get; set; }
    }
}
