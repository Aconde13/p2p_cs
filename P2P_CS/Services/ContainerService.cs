﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Repositories;
using P2P_CS.Models;

namespace P2P_CS.Services
{
    class ContainerService : IContainerService
    {
        public void GenerateContainers()
        {
            ContainerRepository repository = new ContainerRepository();
            for (int i = 1; i <= 15; i++)
            {
                P2PContainer newContainer = repository.CreateContainer(i, 101 + i, "Propertis for ContainerID " + i, 2, 3);
                Console.WriteLine("ContainerID: " + newContainer.ContainerID + " Content en-CA: " + newContainer.Content.Content + " Properties: " + newContainer.Properties + " Is Hero: " + newContainer.IsHero + " Page Type: " + newContainer.P2PPageTypeID);
            }
        }
    }
}
