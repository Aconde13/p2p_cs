﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Repositories;
using P2P_CS.Models;

namespace P2P_CS.Services
{
    class EventService : IEventService
    {
        public void GenerateEvents()
        {
            EventRepository repository = new EventRepository();
            for (int i = 1; i <= 5 ; i++)
            {
                Event eventCreated = repository.CreatEvent(i, 125);
                   Console.WriteLine("EventID: " + eventCreated.EventID);
            }
        }
    }
}
