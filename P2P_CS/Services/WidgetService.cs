﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Repositories;
using P2P_CS.Models;


namespace P2P_CS.Services
{
    class WidgetService : IWidgetService
    {
        public void GenerateWidgets()
        {
            WidgetRepository repository = new  WidgetRepository();
            for (int i = 1; i <= 15; i++)
            {
                P2PWidget newWidget = repository.CreateWidget(i, i + 1, "Properties of widget " + i, 2, "Search", null);
                Console.WriteLine("WidgetID: " + newWidget.WidgetID + " Content fr-CA: " + newWidget.WidgetContentFr.Content +  " Content en-CA: " + newWidget.WidgetContentEn.Content + " Visible: " + newWidget.IsVisible);
            }
        }
    }
}
