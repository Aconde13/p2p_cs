﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Services;

namespace P2P_CS
{
    class Program
    {
        static void Main(string[] args)
        {
            IEventService eventService = new EventService();
            IWidgetService widgetService = new WidgetService();
            IContainerService containerService = new ContainerService();

            Console.WriteLine("--------------------------------------------");
            eventService.GenerateEvents();
            Console.WriteLine("--------------------------------------------");
            widgetService.GenerateWidgets();
            Console.WriteLine("--------------------------------------------");
            containerService.GenerateContainers();
            Console.WriteLine("--------------------------------------------");
        }
    }
}
