﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Models;

namespace P2P_CS.Repositories
{
    class EventRepository
    {
        public Event CreatEvent(int EventID, int OrganizationID)
        {
            Event newEvent = new Event();
            newEvent.EventID = EventID;
            newEvent.OrganizationID = OrganizationID;
            return newEvent;
        }
    }
}
