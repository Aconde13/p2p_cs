﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Models;

namespace P2P_CS.Repositories
{
    class WidgetRepository
    {
        public P2PWidget CreateWidget(int WidgetID, int ContainerID, string Properties, int SortOrder,
            string P2PWidgetTypeID, string VisibilityConditionTypeID)
        {
            P2PWidget widget = new P2PWidget();
            widget.WidgetID = WidgetID;
            widget.ContainerID = ContainerID;
            widget.Locked = false;
            widget.Properties = Properties;
            widget.SortOrder = SortOrder;
            widget.P2PWidgetTypeID = P2PWidgetTypeID;
            widget.IsRequired = false;
            widget.IsVisible = true;
            widget.VisibilityConditionTypeID = VisibilityConditionTypeID;
            widget.WidgetContentEn = CreateWidgetContent("en-CA", WidgetID);
            widget.WidgetContentFr = CreateWidgetContent("fr-CA", WidgetID);
            return widget;
        }

        public P2PWidgetContent CreateWidgetContent(string LanguageCode, int WidgetID)
        {
            P2PWidgetContent newContent = new P2PWidgetContent();
            newContent.WidgetID = WidgetID;
            newContent.LanguageCode = LanguageCode;
            newContent.Content = "{'title': 'NewWidgetContent on " + LanguageCode + " for WidgetID  "+ WidgetID +"'}";
            return newContent;
        }
    }
}