﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P2P_CS.Models;
using P2P_CS.Models.Type;

namespace P2P_CS.Repositories
{
    class ContainerRepository
    {
        public P2PContainer CreateContainer(int ContainerID, int EventID, string Properties, int SortOrder, int ColumnOrder)
        {
            P2PContainer container = new P2PContainer();
            container.ContainerID = ContainerID;
            container.EventID = EventID;
            container.Properties = Properties;
            container.P2PPageTypeID = PageType.Event.ToString();
            container.IsHero = false;
            container.IsHeroLocked = false;
            container.SortOrder = SortOrder;
            container.ColumnOrder = ColumnOrder;
            container.Content = CreateContainerContent(ContainerID, "en-CA");
            return container;
        }

        public P2PContainerContent CreateContainerContent(int ContainerID, string LanguageCode)
        {
            P2PContainerContent newContent = new P2PContainerContent();
            newContent.ContainerID = ContainerID;
            newContent.LanguageCode = LanguageCode;
            newContent.Content = "{'title': 'NewContainerContent on " + LanguageCode + " for ContrainerID: "+ ContainerID +" '}";
            return newContent;
        }
    }
}
